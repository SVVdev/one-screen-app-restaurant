package com.svvdev.veganday;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import static android.icu.lang.UProperty.INT_START;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Find the toolbar view inside the activity layout
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        setSupportActionBar(toolbar);

        // Hide the navigation bar.
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        // setting about fields
        //
        // for CEO
        TextView textViewCEO = findViewById(R.id.ceo_about);
        SpannableStringBuilder strCEO = new SpannableStringBuilder(getString(R.string.ceo_description));
        strCEO.setSpan(new ForegroundColorSpan(Color.GRAY),18,strCEO.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textViewCEO.setText(strCEO);


        // for Cook
        TextView textViewCook = findViewById(R.id.cook_about);
        SpannableStringBuilder strCook = new SpannableStringBuilder(getString(R.string.cook_description));
        strCook.setSpan(new ForegroundColorSpan(Color.GRAY),21,strCook.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textViewCook.setText(strCook);

        // for Supply Manager
        TextView textViewSupmanager = findViewById(R.id.supmanager_about);
        SpannableStringBuilder strSupmanager = new SpannableStringBuilder(getString(R.string.supmanager_description));
        strSupmanager.setSpan(new ForegroundColorSpan(Color.GRAY),30,strSupmanager.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textViewSupmanager.setText(strSupmanager);


    }

    // Menu icons are inflated just as they were with actionbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

}
